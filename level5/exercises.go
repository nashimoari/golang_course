package main

import "fmt"

func ex1() {
	type person struct {
		firstName          string
		lastName           string
		favIceCreamFlavors []string
	}

	person1 := person{
		`Lev`, `Tolstoy`, []string{`strawberry`, `cherry`, `vanilla`},
	}

	person2 := person{
		`Nikolay`, `Gogol`, []string{`Lime`, `Banana`},
	}

	fmt.Println(person1.firstName, person1.lastName)
	for idx, val := range person1.favIceCreamFlavors {
		fmt.Println(idx, ":", val)
	}

	fmt.Println(person2.firstName, person2.lastName)
	for idx, val := range person2.favIceCreamFlavors {
		fmt.Println(idx, ":", val)
	}
}

func ex2() {
	type person struct {
		firstName          string
		lastName           string
		favIceCreamFlavors []string
	}

	type personStorage map[string]person

	person1 := person{
		`Lev`, `Tolstoy`, []string{`strawberry`, `cherry`, `vanilla`},
	}

	person2 := person{
		`Nikolay`, `Gogol`, []string{`Lime`, `Banana`},
	}

	persons := personStorage{
		person1.lastName: person1,
		person2.lastName: person2,
	}

	for idx, val := range persons {
		fmt.Println(idx)
		fmt.Println(val.firstName, val.lastName)
		for idx, val := range val.favIceCreamFlavors {
			fmt.Println(idx, ":", val)
		}
		fmt.Println("========================")
	}
}

func ex3()  {
	type vehicle struct {
		doors int
		color string
	}

	type truck struct {
		vehicle
		fourWheel bool
	}

	type sedan struct {
		vehicle
		luxury bool
	}

	truck1 := truck{
		vehicle: vehicle{doors: 2,
			color: "white",
		},
		fourWheel: true,
	}

	sedan1 := sedan{
		vehicle: vehicle{doors: 4,
			color: "black",
		},
		luxury: true,
	}

	fmt.Println(truck1)
	fmt.Println(truck1.doors)
	fmt.Println(truck1.color)
	fmt.Println(truck1.fourWheel)

	fmt.Println(sedan1)
	fmt.Println(sedan1.doors)
	fmt.Println(sedan1.color)
	fmt.Println(sedan1.luxury)
}

func ex4() {
	myBag := struct {
		brand string
		wheels bool
		pocketsCount uint8
	}{
		brand: "Nike",
		wheels: true,
		pocketsCount: 2,
	}

	fmt.Println(myBag)
}

func main() {
	// ex1()
	// ex2()
	// ex3()
	ex4()
}
