package main

import (
	"fmt"
	"math"
)

func executeFunc(f func(float642 float64), var1 float64){
	f(var1)
}

func main() {

	circleArea := func(radius float64){
		area := math.Pi * math.Pow(radius,2)
		fmt.Println("circle area:", area)
	}

	radius := float64(5)

	executeFunc(circleArea, radius)
}