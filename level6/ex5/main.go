package main

import (
	"fmt"
	"math"
)

type square struct {
	length float64
}

type circle struct {
	radius float64
}

type shape interface {
	calcArea() float64
}

func (c circle) calcArea() float64{
	return math.Pi * math.Pow(c.radius,2)
}

func (sq square) calcArea() float64 {
	return math.Pow(sq.length,2)
}

func info(s shape) {
	fmt.Println(s.calcArea())
}

func main() {


	c1 := circle{5}


	sq1 := square{10}

	fmt.Printf("Circle area: ")
	info(c1)

	fmt.Printf("Square area: ")
	info(sq1)
}
