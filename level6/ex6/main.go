package main

import (
	"fmt"
	"math"
)

func main() {

	radius := float64(5)

	circleArea := func(radius float64) float64{
		return math.Pi * math.Pow(radius,2)
	}(radius)

	fmt.Println(circleArea)

}
