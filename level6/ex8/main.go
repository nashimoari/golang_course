package main

import (
	"fmt"
	"math"
)

func main() {

	getRadiusFunc :=func() func(radius float64) float64{
		return func(radius float64) float64{
			return math.Pi * math.Pow(radius,2)
		}
	}

	circleAreaFunc := getRadiusFunc()

	radius := float64(5)

	fmt.Println("circle area:", circleAreaFunc(radius))
}