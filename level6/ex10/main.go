package main

import "fmt"

func main() {
	x1 := iterator()
	fmt.Println(x1())
	fmt.Println(x1())
	fmt.Println(x1())
	fmt.Println(x1())
	fmt.Println(x1())

	x2 := iterator()
	fmt.Println(x2())
	fmt.Println(x2())
	fmt.Println(x2())
	fmt.Println(x2())
	fmt.Println(x2())
}

func iterator() func() int {
	x := 0
	return func() int {
		x++
		return x
	}
}
