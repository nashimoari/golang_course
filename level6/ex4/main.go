package main

import "fmt"

type user struct {
	first string
	last string
	age uint8
}

func (u user) speak() {
	fmt.Println("I am ", u.first, "and I am", u.age, "years old.")
}


func main() {
	user1 := user{
		"Lev", "Tolstoy", 40,
	}

	user2 := user{
		"Nikolay", "Gogol", 100,
	}

	user1.speak()
	user2.speak()
}
