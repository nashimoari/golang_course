package main

import "fmt"

func foo() int {
	return 7
}

func bar() (int, string) {
	return 42, "The meaning of the everything"
}

func main() {
	fooRes := foo()
	barIntRes, barStrRes := bar()

	fmt.Println(fooRes)
	fmt.Println(barStrRes, ": ", barIntRes)
}
