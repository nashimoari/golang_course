package main

import "fmt"

func foo(data ...int) int {
	sum := 0
	for _, val := range data {
		sum += val
	}
	return sum

}

func bar(data []int) int {
	sum := 0
	for _, val := range data {
		sum += val
	}
	return sum
}

func main() {
	data := []int{1, 2, 3, 4, 5, 6, 7, 8}
	res1 := foo(data...)

	res2 := bar(data)

	fmt.Println(res1)
	fmt.Println("===================")
	fmt.Println(res2)
}
