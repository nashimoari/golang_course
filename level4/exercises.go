package main

import "fmt"

func ex1() {
	arr1 := [5]int{1,2,3,4,5}

	for idx, val := range arr1 {
		fmt.Println(idx, val)
	}

	fmt.Printf("%T", arr1)
}

func ex2() {
	arr1 := []int{1,2,3,4,5,6,7,8,9,10}

	for idx, val := range arr1 {
		fmt.Println(idx, val)
	}

	fmt.Printf("%T", arr1)
}

func ex3() {
	arr1 := []int{42,43,44,45,46,47,48,49,50,51}

	fmt.Println(arr1[:5])
	fmt.Println(arr1[5:])
	fmt.Println(arr1[2:7])
	fmt.Println(arr1[1:6])
}

func ex4() {
	// add some values to slice
	x:= []int{42, 43, 44, 45, 45, 47, 48, 49, 50, 51}
	x = append(x, 52)
	fmt.Println(x)

	x = append(x, 53, 54, 55)
	fmt.Println(x)

	y := []int{56, 57, 58, 59, 60}
	x = append(x, y...)
	fmt.Println(x)
}

func ex5() {
	// delete some values from slice
	x:= []int{42, 43, 44, 45, 45, 47, 48, 49, 50, 51}
	x = append(x[:3], x[6:]...)
	fmt.Println(x)
}

func ex6() {
	states := make([]string, 0, 500)

	states = append(states,
		`Alabama`, `Alaska`, `Arizona`, `Arkansas`, `California`, `Colorado`, `Connecticut`,
		`Delaware`, `Florida`, `Georgia`, `Hawaii`, `Idaho`, `Illinois`, `Indiana`,
		`Iowa`, `Kansas`, `Kentucky`, `Louisiana`, `Maine`, `Maryland`, `Massachusetts`,
		`Michigan`, `Minnesota`, `Mississippi`,
		`Missouri`, `Montana`, `Nebraska`, `Nevada`, `New Hampshire`, `New Jersey`, `New Mexico`, `New York`,
		`North Carolina`, `North Dakota`, `Ohio`, `Oklahoma`, `Oregon`, `Pennsylvania`, `Rhode Island`,
		`South Carolina`, `South Dakota`, `Tennessee`, `Texas`, `Utah`, `Vermont`, `Virginia`, `Washington`,
		`West Virginia`, `Wisconsin`, `Wyoming`)

	for i := 0; i < len(states); i++ {
		fmt.Println(i, states[i])
	}
}

func ex7() {
	persons := [][]string{
			{"James", "Bond", "Shaken, not stirred"},
			{"Miss", "Moneypenny", "Heloooooo, James"},
	}

	for idx, person := range persons {
		fmt.Println("Record: ", idx)
		for idx_val, val := range person {
			fmt.Printf("  index postion: %v \t value: %v \n", idx_val, val)
		}
	}
}

func ex8() {
	p1 := []string{`Shaken, not stirred`, `Martinis`, `Women`}
	p2 := []string{`James Bond`, `Literature`, `Computer Science`}
	p3 := []string{`Being evil`, `Ice cream`, `Sunsets`}

	persons := map[string][]string{
		`bond_james`:p1,
		`moneypenny_miss`:p2,
		`no_dr`:p3,
	}

	for idx, val := range persons {
		fmt.Println(idx, val)
	}

}

func ex9() {
	p1 := []string{`Shaken, not stirred`, `Martinis`, `Women`}
	p2 := []string{`James Bond`, `Literature`, `Computer Science`}
	p3 := []string{`Being evil`, `Ice cream`, `Sunsets`}

	persons := map[string][]string{
		`bond_james`:p1,
		`moneypenny_miss`:p2,
		`no_dr`:p3,
	}

	persons["John"] = []string{"record 1", "record 2", "record 3"}

	for idx, val := range persons {
		fmt.Println(idx, val)
	}
}

func ex10() {
	p1 := []string{`Shaken, not stirred`, `Martinis`, `Women`}
	p2 := []string{`James Bond`, `Literature`, `Computer Science`}
	p3 := []string{`Being evil`, `Ice cream`, `Sunsets`}

	persons := map[string][]string{
		`bond_james`:p1,
		`moneypenny_miss`:p2,
		`no_dr`:p3,
	}

	persons["John"] = []string{"record 1", "record 2", "record 3"}

	delete(persons, "no_dr")

	for idx, val := range persons {
		fmt.Println(idx, val)
	}
}

func main()  {
	// ex1()
	// ex2()
	// ex3()
	// ex4()
	// ex5()
	// ex6()
	// ex7()
	// ex8()
	// ex9()
	ex10()
}
