package main

import (
	"encoding/json"
	"fmt"
	"log"
)

type person struct {
	First  string
	Last   string
	Saying []string
}

func main() {
	p1 := person{
		First:  "James",
		Last:   "Bond",
		Saying: []string{"Shaken, not stirred", "Any last wishes?", "Never say never"},
	}

	bs, err := toJSON(p1)
	if err != nil {
		log.Fatalln("Data can't be marshal to JSON. Error:", err)
	}
	fmt.Println(string(bs))
}

func toJSON(a interface{}) ([]byte, error) {
	bs, err := json.Marshal(a)
	if err != nil {
		return []byte{}, fmt.Errorf("toJSON error %v", err)
	}
	return bs, err
}
