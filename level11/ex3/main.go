package main

import "fmt"

type customErr struct{
	info string
}

func (c customErr) Error() string{
	return c.info
}

func foo (e error) {
	fmt.Println("Error :", e)
}

func main(){
	ce1 := customErr{
		info: "info error string",
	}

	foo(ce1)
}