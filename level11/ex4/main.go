package main

import (
	"fmt"
	"log"
	"math"
)

type sqrtError struct {
	lat  string
	long string
	err  error
}

func (se sqrtError) Error() string {
	return fmt.Sprintf("math error: %v %v %v", se.lat, se.long, se.err)
}

func main() {
	_, err := sqrt(-10.23)
	if err != nil {
		log.Println(err)
	}
}

func sqrt(f float64) (float64, error) {
	if f < 0 {
		// e := errors.New("value MUST be positive")
		e := fmt.Errorf("value MUST be positive. Value was: %v", f)
		return 0, sqrtError{lat: "44.10", long: "133.15", err: e}
	}

	return math.Sqrt(f), nil
}
