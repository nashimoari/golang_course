// This package was created as exercise for Golang course

// Package dog consist special function to make the calculus easier
package dog

// Years takes human years and turns them into dog years
func Years(years uint64) uint64{
	return years * 7
}