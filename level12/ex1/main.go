package main

import (
	"log"
	"temp/level12/ex1/dog"
)

type canine struct {
	name string
	age uint64
}

func main() {

	fido := canine{
		"Fido",
		dog.Years(10),
	}

	log.Println("Age of my dog is", fido.age)
}
