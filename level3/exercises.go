package main

import "fmt"

func ex1() {
	for i:=1; i<=10000; i++ {
		fmt.Println(i)
	}
}

func ex2() {
	for i:=65; i<=90; i++ {
		fmt.Println(i)
		for n:=0; n<3; n++ {
			fmt.Printf("%#U\n", i)
		}
	}
}

func ex3() {
	bd := 1982
	i := bd
	for i <= 2021 {
		fmt.Println(i)
		i++
	}
}

func ex4()  {
	bd := 1982
	i := bd
	for {
		i++
		fmt.Println(i)
		if i == 2021 {
			break
		}
	}
}

func ex5() {
	for i:=10; i<=100; i++ {
		if i%4 == 0 {
			fmt.Println(i)
		}
	}
}

func ex6() {
	if 42 == 42 {
		fmt.Println("42 is equal 42")
	}
}

func ex7() {
	x := 42
	if x == 41 {
		fmt.Println("x is 41")
	} else if x == 42 {
		fmt.Println("x is 42")
	} else {
		fmt.Println("x is something else")
	}
}

func ex8()  {
	switch {
		case true: {
			fmt.Println("it's true")
		}

		case false: {
			fmt.Println("it's false")
		}
	}
}

func ex9()  {
	favSport := "Tennis"

	switch favSport {
		case "Tennis": {
			fmt.Println("Tennis")
		}

		case "Football": {
			fmt.Println("Football")
		}

		case "Soccer": {
			fmt.Println("Soccer")
		}
	}
}

func ex10()  {
	fmt.Println(true && true)
	fmt.Println(true && false)
	fmt.Println( true || true)
	fmt.Println(true || false)
	fmt.Println(!true)
}

func main()  {
	// ex1()
	// ex2()
	// ex3()
	// ex4()
	// ex5()
	// ex6()
	// ex7()
	// ex8()
	// ex9()
	ex10()
}