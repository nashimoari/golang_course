# Level 13

Get the code ready to BET on the code (add Benchmarks, examples, tests)

You should add file *_test.go

## Benchmarking
You should add function for benchmarking your code.

### Run benchmarking
go test -bench .

## Examples
You should add function for examples

## Testing
You should add function for test your code

### Run tests
go test

### Coverage

go test -coverprofile c.out

go tool cover -html=c.out
