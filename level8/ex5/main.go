package main

import (
	"fmt"
	"sort"
)

type user struct {
	First   string
	Last    string
	Age     int
	Sayings []string
}

type ByLast []user

type ByAge []user

func main() {
	u1 := user{
		First: "James",
		Last:  "Bond",
		Age:   32,
		Sayings: []string{
			"Shaken, not stirred",
			"Youth is no guarantee of innovation",
			"In his majesty's royal service",
		},
	}

	u2 := user{
		First: "Miss",
		Last:  "Moneypenny",
		Age:   27,
		Sayings: []string{
			"James, it is soo good to see you",
			"Would you like me to take care of that for you, James?",
			"I would really prefer to be a secret agent myself.",
		},
	}

	u3 := user{
		First: "M",
		Last:  "Hmmmm",
		Age:   54,
		Sayings: []string{
			"Oh, James. You didn't.",
			"Dear God, what has James done now?",
			"Can someone please tell me where James Bond is?",
		},
	}

	users := []user{u1, u2, u3}

	fmt.Println(users)

	// your code goes here
	fmt.Println("======================")
	sort.Sort(ByLast(users))

	fmt.Println(users)

	fmt.Println("======================")
	sort.Sort(ByAge(users))
	fmt.Println(users)

}


func (users ByLast) Len() int {
	return len(users)
}

func (users ByLast) Swap(i, j int) {
	users[i], users[j] = users[j], users[i]
}

func (users ByLast) Less(i, j int) bool {
	return users[i].First < users[j].First
}


func (users ByAge) Len() int {
	return len(users)
}

func (users ByAge) Swap(i, j int) {
	users[i], users[j] = users[j], users[i]
}

func (users ByAge) Less(i, j int) bool {
	return users[i].Age < users[j].Age
}