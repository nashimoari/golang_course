package main

import (
	"encoding/json"
	"fmt"
	"os"
)

type user struct {
	First string
	Age   int
}

func main() {
	u1 := user{
		First: "James",
		Age:   32,
	}

	u2 := user{
		First: "Moneypenny",
		Age:   27,
	}

	u3 := user{
		First: "M",
		Age:   54,
	}

	users := []user{u1, u2, u3}

	var users2 []user

	fmt.Println(users)

	// Exercise1: Marshall data to []uint8
	jsonData, _ := json.Marshal(users)

	fmt.Println(jsonData)
	// if we want to see the text
	fmt.Println(string(jsonData))
	fmt.Printf("%T\n", jsonData)

	// Exercise2: Unmarshall json string
	fmt.Println("============")
	err := json.Unmarshal(jsonData, &users2)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(users2)

	// Exercise3: Encode data to json and send it to StdOut
	fmt.Println("=================")
	err2 := json.NewEncoder(os.Stdout).Encode(users)
	if err2 != nil {
		fmt.Println(err2)
	}

}