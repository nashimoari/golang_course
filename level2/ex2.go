package main

import (
	"fmt"
)

const (
	year1 = 2021 - iota
	year2 = year1 - iota
	year3 = year1 - iota
	year4 = year1 - iota
 c1 = "test"
 c2 int = 8

)

var x int

func main() {
	// Ex 2.1
	x = 42

	fmt.Printf("%d %b %#x\n", x, x, x)

	// Ex2.2
	a := 42 == 43
	b := 42 <= 43
	c := 42 >= 43
	d := 42 < 43
	e := 42 > 43

	fmt.Println(a, b, c, d, e)

	// Ex2.3
	fmt.Println(c1, c2)

	// Ex2.4
	x_shifted := x << 1
	fmt.Printf("%d %b %#x\n", x_shifted, x_shifted, x_shifted)

	// Ex2.5
	v25 := `Hello!
it's something text for the exercise.
Bye!`
	fmt.Println(v25)

	// Ex2.6
	fmt.Println(year1, year2, year3, year4)
}