package word

import (
	"fmt"
	"temp/level13/ex2/quote"
	"testing"
)

func TestUseCount(t *testing.T) {
	res := UseCount("one two three three three")
	for k, v := range res {
		switch k {
		case "one":
			if v != 1 {
				t.Error("got", v, "want 1")
			}

		case "two":
			if v != 1 {
				t.Error("got", v, "want 1")
			}

		case "three":
			if v != 3 {
				t.Error("got", v, "want 3")
			}
		}
	}

}

func TestCount(t *testing.T) {
	res := Count("one two three")
	if res != 3 {
		t.Error("Got", res, "want 3")
	}
}

func ExampleCount() {
	fmt.Println(Count("one two three"))
	// Output:
	// 3
}

func BenchmarkCount(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Count(quote.SunAlso)
	}

}

func BenchmarkUseCount(b *testing.B) {
	for i := 0; i < b.N; i++ {
		UseCount(quote.SunAlso)
	}
}
