package main

import (
	"fmt"
	"strings"
)

type person struct {
	firstName string
	lastName string
}

func changeMe(p *person) {
	p.lastName = strings.ToUpper(p.lastName)
}

func main() {
 p1 := person{
 	"Ivan",
 	"Petrov",
 }

 changeMe(&p1)

 fmt.Println(p1)
}
