// fix race condition example from ex3
// go run --race main.go

package main

import (
	"fmt"
	"runtime"
	"sync"
	"sync/atomic"
)

var wg = sync.WaitGroup{}

var i int64

func incrementer() {
	defer wg.Done()

	runtime.Gosched()

	atomic.AddInt64(&i, 1)

	fmt.Println(atomic.LoadInt64(&i))
}

func main() {

	for i:=0; i<100; i++ {
		go incrementer()
		wg.Add(1)
	}


	wg.Wait()
	fmt.Println("Finish value: ", i)
}
