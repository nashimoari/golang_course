package main

import (
	"fmt"
	"runtime"
	"sync"
	"time"
)

var wait sync.WaitGroup


func printMarco() {
	defer wait.Done()

	for i:=0;i<10;i++ {
		fmt.Println("Marco")
		time.Sleep(time.Second * 1)
	}
}

func printPolo() {
	defer wait.Done()

	for i:=0;i<10;i++ {
		fmt.Println("Polo")
		time.Sleep(time.Second * 1)
	}
}

func printInfo(prefix string) {
	fmt.Println(prefix, "CPU", runtime.NumCPU())
	fmt.Println(prefix, "gs", runtime.NumGoroutine())
}

func main() {

	fmt.Println("Starting go routines...")
	printInfo("starting")

	go printMarco()
	wait.Add(1)

	go printPolo()
	wait.Add(1)

	fmt.Println("go routines started")
	printInfo("started")

	wait.Wait()

	fmt.Println("go routines finished")
	printInfo("finished")
}
