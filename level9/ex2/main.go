package main

import "fmt"

type person struct {
	first string
	second string
}

func (p *person) speak(speech string) {
	fmt.Println(p.first, "speak", speech)
}

type human interface {
	speak(speech string)
}

func saySomething(h human, speech string) {
	h.speak(speech)
}

func main() {
	p1 := person{
		"Ivan",
		"Petrov",
	}

	p1.speak("test 1 2 3")

	saySomething(&p1, "Pow wow")

	// I can't do this because method speak receive only pointer as receiver
	// saySomething(p1, "Abc")
}
