// race condition example
// go run --race main.go

package main

import (
	"fmt"
	"runtime"
	"sync"
)

var wg = sync.WaitGroup{}

var i = 0

func incrementer() {
	defer wg.Done()
	v := i
	runtime.Gosched()
	v++
	i = v
	fmt.Println(i)
}

func main() {

	for i:=0; i<100; i++ {
		go incrementer()
		wg.Add(1)
	}


	wg.Wait()
	fmt.Println("Finish value: ", i)
}
