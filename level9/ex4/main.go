// fix race condition example from ex3
// go run --race main.go

package main

import (
	"fmt"
	"runtime"
	"sync"
)

var wg = sync.WaitGroup{}
var m1 = sync.Mutex{}

var i = 0

func incrementer() {
	defer wg.Done()

	m1.Lock()

	v := i
	runtime.Gosched()

	v++
	i = v

	fmt.Println(i)
	m1.Unlock()
}

func main() {

	for i:=0; i<100; i++ {
		go incrementer()
		wg.Add(1)
	}


	wg.Wait()
	fmt.Println("Finish value: ", i)
}
