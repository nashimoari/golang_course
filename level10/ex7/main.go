package main

import (
	"fmt"
	"sync/atomic"
	"time"
)

var workingRoutines int64

func main() {
	c := make(chan int)

	for i := 0; i < 10; i++ {
		go func(i int) {
			for n := 0; n < 10; n++ {
				c <- i*10 + n
			}
			atomic.AddInt64(&workingRoutines, -1)
		}(i)
		workingRoutines++
	}

	go controlRoutine(c)

	for val := range c {
		fmt.Println(val)
	}
}

func controlRoutine(c chan int) {
	for {
		time.Sleep(time.Second)
		if workingRoutines > 0 {
			continue
		}
		close(c)

	}
}
