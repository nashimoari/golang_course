package main

import (
	"fmt"
	"sync"
	"time"
)

var wg = sync.WaitGroup{}

var c = make(chan int)

func chanSend(c chan int, val int) {
	defer wg.Done()

	// !!! CHANNELS BLOCK
	c <- val

	// This message will print at the end. AFTER message read from channel
	// We can prevent this behavior using buffered channel (see at ex1_v2)
	fmt.Println("release block")
}

func main() {
	go chanSend(c, 42)
	wg.Add(1)

	fmt.Println("go sleep")
	time.Sleep(time.Second * 2)
	fmt.Println("Awake")
	fmt.Println(<-c)

	wg.Wait()

}
