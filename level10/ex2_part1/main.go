package main

import "fmt"

func main() {
	cs := make(chan int)

	// we can't read from send-only channel
	// cs := make(chan <- int)

	go func() {
		cs <- 42
	}()

	fmt.Println(<-cs)

	fmt.Printf("------\n")
	fmt.Printf("cs\t%T\n", cs)

}
