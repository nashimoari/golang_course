package main

import "fmt"

func main() {
	//bufferedExample()
	parallelExample()
}

func parallelExample() {
	c := make(chan int)
	go func() {
		for i := 0; i < 100; i++ {
			c <- i
		}
		close(c)
	}()

	for val := range c {
		fmt.Println(val)
	}
}

func bufferedExample() {
	c := make(chan int, 100)

	for i := 0; i < 100; i++ {
		c <- i
	}

	for len(c) > 0 {
		fmt.Println(<-c)
	}
}
