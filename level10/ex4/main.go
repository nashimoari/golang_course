package main

import "fmt"

var q = make(chan int)

func main() {
	c := gen()

	receive(c)

	fmt.Println("about to exit")
}

func gen() <-chan int {
	c := make(chan int)

	go func() {
		for i := 0; i < 100; i++ {
			c <- i
		}
		fmt.Println("send message to quit")
		q <- 1
	}()
	return c
}

func receive(c <-chan int) {
	for {
		select {
		case val := <-c:
			fmt.Println(val)
		case <-q:
			fmt.Println("quit")
			return
		}
	}
}
